var express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose');

// Constants
var port = 8887,
    mongoUri = 'mongodb://localhost:27017/ngTemplate';

// App Definitions
var app = express();

  app.use(express.static(__dirname + '/public'));

  // Middleware
  app.use(cors());

  // Expanding server capacity
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true}));

  // Controllers

  ////////////////////////////////////////////////////////
  ////                    Rest API                    ////
  ////////////////////////////////////////////////////////

  // Various APIs

  // Connect mongoose
  mongoose.connect(mongoUri);
  mongoose.connection.once('open', function() {
    console.log('Connected on ', mongoUri);
  });

  app.listen(port, function() {
    console.log('Do or do not, there is no try', port);
  });
